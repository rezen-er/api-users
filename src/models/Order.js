const mongoose = require('mongoose')
const validator = require('validator')

/*
{
  "orderId": ""
  "total": 21,
  "status": "WAITING",
  "restaurantId": "",
  "customerId": "",
  "items": [
    {
      "price": 10.5,
      "description": "",
      "quantity": 2,
      "name": ""
    }
  ],
  "fullname": "Lucas Michelini Reis de Oliveira",
  "email": "lucasmro@gmail.com",
  "phone": "11986115678",
  "delivery": {
    "state": "SP",
    "street": "Rua Guararapes",
    "city": "São Paulo",
    "zip": "04561-000"
  }
}
*/

const item = mongoose.Schema({ 
    name: String,
    price: Number,
    description: String,
    quantity: Number
 });

const orderSchema = mongoose.Schema({
    orderId: {
        type: Number,
        required: true,
        trim: true
    },
    total: {
        type: Number,
        required: true,
        trim: true
    },
    customerId: {
        type: mongoose.ObjectId,
        required: true,
        trim: true
    },
    status: {
        type: String,
        required: true,
        trim: true
    },
    items: {
        type: [item],
        required: true,
    },
    customerName: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        lowercase: true,
        validate: value => {
            if (!validator.isEmail(value)) {
                throw new Error({error: 'Invalid Email address'})
            }
        }
    },
    phone: {
        type: Number,
    }
}, {
    timestamps: true
})

const order = mongoose.model('Order', orderSchema)

module.exports = order