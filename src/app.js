const express = require('express')
const userRouter = require('./routers/user')
const orerRouter = require('./routers/order')
const port = process.env.PORT
require('./db/db')

const app = express()

app.use(express.json())

// A sample route
app.get('/', (req, res) => res.send('working!'))

app.use(userRouter)
app.use(orerRouter)

// Start the Express server
app.listen(port, () => console.log('Server running on port ' + port))