const express = require('express')
const Order = require('../models/Order')

const router = express.Router()

router.post('/order', async (req, res) => {
    // Create a new user
    try {
        const order = new Order(req.body)
        await order.save()
        res.status(201).send({ order })
    } catch (error) {
        res.status(400).send(error)
    }
})

module.exports = router